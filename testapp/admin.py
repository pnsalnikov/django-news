from django.contrib import admin
from testapp.models import Rubric
from mptt.admin import MPTTModelAdmin
# Register your models here.

admin.site.register(MPTTModelAdmin, Rubric)